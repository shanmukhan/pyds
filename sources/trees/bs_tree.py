"""
    Author: Shanmukhan
    Date:   09-08-2019 18:48
    Desc: 
"""
from sources.trees.bt_node import BinaryNode


class BinarySearchTree:

    def __init__(self):
        self.root = None
        self.depth = 0

    def add(self, data):
        if not self.root:
            self.root = BinaryNode(data)
            self.root.depth = 0
        else:
            self._add(data, self.root)

    def _add(self, data, node):
        if data > node.data:
            if node.right:
                self._add(data, node.right)
            else:
                new_node = BinaryNode(data)
                new_node.depth = node.depth + 1
                node.right = new_node
                self.depth = max(self.depth, new_node.depth)
        elif data < node.data:
            if node.left:
                self._add(data, node.left)
            else:
                new_node = BinaryNode(data)
                new_node.depth = node.depth + 1
                node.left = new_node
                self.depth = max(self.depth, new_node.depth)
        else:
            print('Wrong entry', node)

    def bfs(self):
        print('Depth is', self.depth)
        q = [self.root]
        idx = 0
        level = 0
        ele_cnt = 1
        while q:
            node = q.pop(0) or None
            if level > self.depth:
                print('self.depth', self.depth, 'level', level, 'node', node)
                # for n in q:
                #     print(n)
                break

            if idx == 0:
                print(' ' * (2 ** (self.depth - level) - 1), node or ' ', sep='', end='')
            else:
                print(' ' * (2 ** (self.depth - level + 1) - 1), node or ' ', sep='', end='')
            idx += 1
            if ele_cnt == (2 ** (level + 1)) - 1:
                level += 1
                idx = 0
                print('')
            if node:
                q.append(node.left)
            if node:
                q.append(node.right)
            ele_cnt += 1

    def height(self):
        return self._height(self.root)

    def _height(self, node):
        if node:
            lh = self._height(node.left) + 1
            rh = self._height(node.right) + 1
            print('lh', lh, 'rh', rh)
        else:
            return -1
        return max(lh, rh)

    def in_order_dfs(self):
        print("In order results")
        self._in_order_dfs(self.root)

    def _in_order_dfs(self, node):
        if node:
            self._in_order_dfs(node.left)
            print(node)
            self._in_order_dfs(node.right)

    def delete(self, val):
        if self.root and self.root.data == val and not self.root.left and not self.root.right:
            print('Root node is delted', self.root)
            self.root = None
        else:
            self._delete(val)

    def _delete(self, val):
        p = self.find_parent(val)
        n, side = (p.left, 'left') if p.left and p.left.data == val else (p.right, 'right')
        print('Parent of', val, 'is', p, 'side is', side)
        if not n.left and not n.right:
            if side == 'left':
                p.left = None
            else:
                p.right = None
        elif n.left and n.right:
            small = self.smallest_in_right_node(n)
            p_small = self.find_parent(small.data)
            n.data = small.data
            if p_small.left:
                p_small.left = None
            else:
                p_small.right = None
        elif n.left:
            p.left = n.left
        elif n.right:
            p.right = n.right

    def find_parent(self, val):
        if self.root and self.root.data == val:
            return self
        return self._find_parent(val, self.root)

    def _find_parent(self, val, node):
        if node.left:
            if node.left.data == val:
                return node
            else:
                p = self._find_parent(val, node.left)
                if p:
                    return p
        if node.right:
            if node.right.data == val:
                return node
            else:
                p = self._find_parent(val, node.right)
                if p:
                    return p

    def smallest_in_right_node(self, node):
        tmp = node.right
        while tmp and tmp.left:
            tmp = tmp.left
        print('Smallest element of tree', node, 'is', tmp)
        return tmp


def add_tree(nums, tree):
    if nums:
        mid_ = len(nums) // 2
        left, mid, right = nums[:mid_], nums[mid_], nums[mid_ + 1:]
        print(left, mid, right, mid_)
        tree.add(mid)
        add_tree(left, tree)
        add_tree(right, tree)


def main():
    tree = BinarySearchTree()
    nums = list(range(10))
    add_tree(nums, tree)

    tree.bfs()
    tree.in_order_dfs()
    dep = tree.height()
    print('Depth is', dep)
    print('Parent of 1 is', tree.find_parent(1))
    print('Parent of 9 is', tree.find_parent(9))
    print('Parent of 6 is', tree.find_parent(6))
    print('Parent of 5 (root) is', tree.find_parent(5))
    tree.bfs()
    tree.delete(2)
    tree.bfs()


if __name__ == '__main__':
    main()
