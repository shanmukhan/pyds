"""
    Author: Shanmukhan
    Date:   09-08-2019 18:35
    Desc: 
"""

import logging


class BinaryNode:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None
        self.depth = None
        self.logger = logging.getLogger(self.__class__.__name__)

    def __str__(self):
        return '%s' % self.data

    def __repr__(self):
        return "<%s - data=%s, depth=%s>" % (self.__class__.__name__, self.data, self.depth)