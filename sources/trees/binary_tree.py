from queue import Queue


class BTree(object):
    def __init__(self):
        self.root = None

    def add(self, data):
        if not self.root:
            self.root = BNode(data)
        else:
            self.add_child(self.root, data)

    def add_child(self, node, data):
        if data < node.data:
            if node.left:
                self.add_child(node.left, data)
            else:
                node.left = BNode(data)
        elif data > node.data:
            if node.right:
                self.add_child(node.right, data)
            else:
                node.right = BNode(data)

    def bfs(self):
        if not self.root:
            print('Empty Tree')
            return
        q = Queue()
        q.put(self.root)
        while q.qsize():
            node = q.get(False)
            q.put(node.left)
            q.put(node.right)
            print(node.data)

    def depth(self):
        pass

    def dfs(self):
        node = self.root


class Node(object):

    def __init__(self, data):
        self.data = data
        self.next = None


class BNode(object):
    def __init__(self, data):
        self.data = data
        self.right = None
        self.left = None
