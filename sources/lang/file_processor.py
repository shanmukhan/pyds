"""
    Author: Shanmukhan
    Date:   07-05-2019 23:12
    Desc: 
"""

import logging
import re
import os
from os import listdir


class FileProcessor(object):

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        # with open('C:/Users/shanm/Downloads/Logs/OOE_JST_Orders_XML_20190505202801.log', 'r') as file:
        #     self.data = file.read()
        self.out_file = None

    def processFiles(self, files):
        self.out_file = open('C:/Users/shanm/Downloads/Logs/output.csv', 'w')
        self.out_file.write("BatchId,Order No,No. of Stores,Processing time\n")
        for file in files:
            p.processFile(file)
        self.out_file.close()

    def processFile(self, fileName):
        file = open(fileName, 'r')
        time_map = {}
        stores_map = {}
        all_orders = []
        cur_order = None
        for line in file:
            # all_orders = re.findall('\| All orders \[(.+)\]', line)
            # if all_orders:
            #     all_orders = eval('['+all_orders[0].replace(' ', ',')+']')
            #     print('All Orders are', all_orders)
            # else:
            order = re.findall('Processing order (.+) \(', line)
            if order:
                cur_order = order[0]
                all_orders.append(cur_order)
            else:
                stores = re.findall('\| ([0-9]+) stores \([0-9]+ unique\)', line)
                if stores:
                    stores_map[cur_order] = stores[0]
                else:
                    secs = re.findall('--- Completed order %s in (.+) seconds ---'%cur_order, line)
                    if secs:
                        time_map[cur_order] = secs[0]+'sec'
        print('Time map', time_map)
        print('Stores map', stores_map)
        print('All Orders', all_orders)
        file.close()
        log_name = fileName
        for order in all_orders:
            text = "%s,%s,%s,%s\n" % (os.path.basename(log_name), order, stores_map.get(order), time_map.get(order))
            self.out_file.write(text)
            log_name = ''


if __name__ == '__main__':
    dirName = 'C:/Users/shanm/Downloads/Logs'
    files = listdir(dirName)
    files = [dirName+'/'+file for file in files if file.endswith('.log')]
    p = FileProcessor()
    # files = ['C:/Users/shanm/Downloads/Logs/OOE_JST_Orders_XML_20190505202801.log']
    p.processFiles(files)

