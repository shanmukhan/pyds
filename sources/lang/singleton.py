"""
    Author: Shanmukhan
    Date:   10/7/2018 10:55 PM
    Desc: 
"""

import logging


class Singleton:

    _instance = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(Singleton._instance, cls):
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)


if __name__ == '__main__':
    s1 = Singleton()
    s2 = Singleton()
    print('S1=%s is S2=%s => %s' % (s1, s2, s1 is s2))
    Singleton._instance = None
    s2 = Singleton()
    print('S1=%s is S2=%s => %s' % (s1, s2, s1 is s2))
    # print(s1.__dict__)
