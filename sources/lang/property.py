"""
    Author: Shanmukhan
    Date:   11/18/2018 11:12 AM
    Desc: 
"""

import logging


class Property:

    def __init__(self):
        self._isSowDone = False
        self.logger = logging.getLogger(self.__class__.__name__)

    @property
    def sow_done(self):
        return self._isSowDone

    @sow_done.setter
    def sow_done(self, value):
        self._isSowDone = value


if __name__ == '__main__':
    prop = Property()
    print(prop.sow_done)
    prop.sow_done = True
    print(prop.sow_done)

