"""
    Author: Shanmukhan
    Date:   10/7/2018 10:59 PM
    Desc: 
"""

import logging


class Dunder:
    __dunder = 'Dunder Super'

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)


if __name__ == '__main__':
    d1 = Dunder()
    print(d1.__dict__)
    print(d1._Dunder__dunder)
    print(Dunder._Dunder__dunder)
    Dunder._Dunder__dunder = 'Modified'
    print(d1.__dunder)
