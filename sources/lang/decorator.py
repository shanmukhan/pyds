"""
    Author: Shanmukhan
    Date:   10/13/2018 5:12 PM
    Desc: Decorators gets executed even before they are called,
            they gets called while module is loaded
"""

import logging


def pre(func):
    print('Pre sate of of func', func)
    return func


class DecoratorTest:

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    @pre
    def test(self):
        print('Executing test method')

    def wout_dec(self):
        print('without decorator')


def main():
    t1 = DecoratorTest()
    t1.wout_dec()
    t1.test()
    t1.test()
    t1.test()


if __name__ == '__main__':
    main()
